const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/mongotechu/collections/";
const mLabAPIKey = "apiKey=6DgqdPnxkqpqkbiShQMs6QgujOamf0pV";

function loginV3(req, res){
  console.log("POST /apitechu/v3/login");
  console.log("email es " + req.body.email);
  console.log("password es " + req.body.password);

  var query = 'q={"email": "' + req.body.email +'"}';
  var httpClient = requestJson.createClient(baseMLabURL);

  httpClient.get("usuario?" + query + "&" + mLabAPIKey,
    function (err, resMLab, bodyGet){
      if (err) {
        response = {
          "msg" : "Error obteniendo usuario"
        }
        res.status(500);
        res.send(response);
      } else {
        if (bodyGet.length > 0) {
          var response = bodyGet[0];
          console.log("Entro para comprobar el password y actualizar el user");
          if (crypt.checkPassword(req.body.password,response.password)){
            console.log("Password correcta");
            var putBody = '{"$set":{"logged":true}}';
            httpClient.put("usuario?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
              function (err, resMLab, bodyPut){
                console.log("Entro en el put");
                if (err) {
                  response = { "msg" : "Error obteniendo usuario" }
                  res.status(500);
                  console.log(response);
                  res.send(response);
                } else {
                  response = {
                              "msg" : "login correcto",
                              "id"  : bodyGet[0].id_usuario
                            };
                            console.log(response);
                            res.send(response);
                }
              }
            )
          }else{
            console.log("Password Incorrecta");
            var response = {
              "msg" : "Password Incorrecta"
            }
            res.status(403);
            res.send(response);
          }
        } else {
          var response = {
            "msg" : "Usuario no encontrado"
          }
          res.status(404);
          res.send(response);
        }
      }

    }
  )
}

////LOGIN V2 DEL profe
/*
function loginV2(req, res) {
 console.log("POST /apitechu/v2/login");
 var email = req.body.email;
 var password = req.body.password;

 var query = 'q={"email": "' + email + '"}';
 console.log("query es " + query);

 httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {

     var isPasswordcorrect =
       crypt.checkPassword(password, body[0].password);
     console.log("Password correct is " + isPasswordcorrect);

     if (body.length == 0 || !isPasswordcorrect) {
       var response = {
         "mensaje" : "Login incorrecto, email y/o passsword no encontrados"
       }
       res.send(response);
     } else {
       console.log("Got a user with that email and password, logging in");
       query = 'q={"id" : ' + body[0].id +'}';
       console.log("Query for put is " + query);
       var putBody = '{"$set":{"logged":true}}';
       httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT done");
           var response = {
             "msg" : "Usuario logado con éxito",
             "idUsuario" : body[0].id
           }
           res.send(response);
         }
       )
     }
   }
 );
}
*/

function logoutV3(req, res){
  console.log("POST /apitechu/v3/logout/:id");

  var id = req.params.id;
  console.log(id);
  var query  = 'q={"id_usuario":' + id + '}';

  var putBody = '{"$unset":{"logged":""}}';
  var httpClient = requestJson.createClient(baseMLabURL);

  httpClient.get("usuario?" + query + "&" + mLabAPIKey,
    function (err, resMLab, bodyGet){
      if (err) {
        response = {
          "msg" : "Error obteniendo usuario"
        }
        res.status(500);
        res.send(response);
      } else {
        if (bodyGet.length > 0) {
          var response = bodyGet[0];
          console.log("Entro para actualizar el user y quitar el logged");
            if (bodyGet[0].logged){
            httpClient.put("usuario?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
              function (err, resMLab, bodyPut){
                console.log("Entro en el put");
                if (err) {
                  response = { "msg" : "Error obteniendo usuario" }
                  res.status(500);
                  console.log(response);
                  res.send(response);
                } else {
                  response = {
                              "msg" : "logout correcto",
                              "id"  : bodyGet[0].id_usuario
                            };
                            console.log(response);
                            res.send(response);
                }
              }
            );
          }else{
            response = { "msg" : "El usuario no estaba logado" };
            res.send(response);
          }
          } else {
            var response = {"msg" : "logout incorrecto" }
          res.status(404);
          res.send(response);
        }
      }
    }
  )
}

//LOGOUT V2 por el profe
/*
function logoutV2(req, res) {
 console.log("POST /apitechu/v2/logout/:id");

 var query = 'q={"id": ' + req.params.id + '}';
 console.log("query es " + query);

 httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
     if (body.length == 0) {
       var response = {
         "mensaje" : "Logout incorrecto, usuario no encontrado"
       }
       res.send(response);
     } else {
       console.log("Got a user with that id, logging out");
       query = 'q={"id" : ' + body[0].id +'}';
       console.log("Query for put is " + query);
       var putBody = '{"$unset":{"logged":""}}'
       httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT done");
           var response = {
             "msg" : "Usuario deslogado",
             "idUsuario" : body[0].id
           }
           res.send(response);
         }
       )
     }
   }
 );
}
*/

module.exports.loginV3 = loginV3;
module.exports.logoutV3 = logoutV3;
