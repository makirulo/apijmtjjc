const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/mongotechu/collections/";
const mLabAPIKey = "apiKey=6DgqdPnxkqpqkbiShQMs6QgujOamf0pV";

/////VERSION3/////

function getMovementsV3(req, res){
  console.log("GET /apitechu/v3/movements");

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Movements");
  httpClient.get("movimiento?" + mLabAPIKey,
    function (err, resMLab, body){
      var response = !err ? body : {
        "msg" : "Error obteniendo movimientos."
      }
      res.send(response);
    }
  );
}

function getMovementByIdV3(req, res){
  console.log("GET /apitechu/v3/movements/:id");

  var id = req.params.id;
  //'' comillas simples es un String literal
  var query  = 'q={"id_movimiento":' + id + '}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Movement");
  httpClient.get("movimiento?" + query + "&" + mLabAPIKey,
    function (err, resMLab, body){
      if (err) {
        response = {
          "msg" : "Error obteniendo movimiento"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body[0];
          //Por defecto el status es 200
        } else {
          var response = {
            "msg" : "Movimiento no encontrado"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  );
}


function createMovementV3(req, res) {
  console.log("POST /apitechu/v3/movement/");
  //el id de la cuenta en el que quiero insertar un movimiento.
  var fechaMovimiento = new Date();
  var bodyMovementOrigen = {
    //menos el id movimiento que lo asignaremos via query y el cuenta id lo demás viene por el body
    "id_movimiento": 0,
   "fecha": fechaMovimiento,
   "importe": req.body.importe,
   "cuenta_id": parseInt(req.body.cuenta_id_origen),
   "descripcion": req.body.descripcion,
   "categoria": req.body.categoria,
   "tipo_movimiento": req.body.tipo_movimiento,

  };
  var saldoMovimiento = req.body.importe;
  var idCuentaOrigen = req.body.cuenta_id_origen;

  if (req.body.tipo_movimiento == "traspaso"){
    //GENERAMOS EL JSON DE DESTINO
    bodyMovementOrigen.importe=req.body.importe*(-1);
    var idCuentaDestino = req.body.cuenta_id_destino; //cambiamos el sentido del movimiento cuando es traspaso
    var bodyMovementDestino = {
      //menos el id movimiento que lo asignaremos via query y el cuenta id lo demás viene por el body
      "id_movimiento": 0,
     "fecha": fechaMovimiento,
     "importe": req.body.importe,
     "cuenta_id": parseInt(req.body.cuenta_id_destino),
     "descripcion": req.body.descripcion,
     "categoria": req.body.categoria,
     "tipo_movimiento": req.body.tipo_movimiento,
     };
 }



  console.log("id_cuenta_origen: " + idCuentaOrigen);

  var query = 'q={"id_cuenta":' + parseInt(idCuentaOrigen) + '}&f={"id_cuenta":1,"saldo":1, "_id":1}';
  clienteMlab = requestJson.createClient(baseMLabURL + "/cuenta?" + query + "&l=1&" + mLabAPIKey);
  clienteMlab.get('', function(err, resM, body)
    {
      if(!err)
      {
        if(body.length == 1)
        {
          var idCuenta = body[0]._id.$oid;
          console.log("Se procede al registro del movimiento en la cuenta existente");
          var query = 'q={"_id":{$exists: true }}&f={"id_movimiento":1,"cuenta_id":1}&s={"id_movimiento":-1}&l=1';
          var saldoCuenta = body[0].saldo;
          console.log();

          //Consigue en max id ordenando descendentemente
          clienteMlab = requestJson.createClient(baseMLabURL + "/movimiento?" + query + "&" + mLabAPIKey);
          clienteMlab.get('', function(errX, resX, bodyX)
          {
            if(!errX)
            {
              if(bodyX.length == 1)
              {
                var idNuevoMovimiento = bodyX[0].id_movimiento + 1;
                console.log("idNuevoMovimiento:" + idNuevoMovimiento);
                bodyMovementOrigen.id_movimiento = idNuevoMovimiento;
                var saldoMovimiento = bodyMovementOrigen.importe;

                console.log("importe movimiento " + saldoMovimiento);
                console.log("saldo de la cuenta " + saldoCuenta);

                clienteMlab = requestJson.createClient(baseMLabURL);

                clienteMlab.post("movimiento?" + mLabAPIKey, bodyMovementOrigen,
                  function (errP, resP, bodyP){
                    if(!errP)
                    {
                      console.log("Movimiento creado con exito");

                      //Actualizo el saldo de la cuenta afectada
                      clienteMlab = requestJson.createClient(baseMLabURL);
                      var saldoFinal = saldoMovimiento + saldoCuenta;

                      console.log("Saldo Cuenta Antes del movimiento"+ saldoCuenta);
                      console.log("Saldo Cuenta Update es "+ saldoFinal.toString());

                      var putBody = '{"$set" : {"saldo":'+ saldoFinal.toString() +'}}';

                      console.log(putBody);
                      console.log("la cuenta que se actualiza tiene id "+idCuenta);

                      clienteMlab.put("cuenta/" + idCuenta + "?"+ mLabAPIKey, JSON.parse(putBody),
                      function (errP2, resP2, bodyP2)
                      {
                        if(!errP2)
                        {
                          console.log("Cuenta actualizada con exito");

                        }else{
                          res.status(404).send("Error actualizando saldo de cuenta");
                        }
                      });
                      if (req.body.tipo_movimiento == "traspaso")
                      {
                          console.log({"msg" : "Movimiento origen creado con exito"});
                          //no hago el res.send para insertar el segundo movimiento
                      }else {
                        res.send({"msg" : "Movimiento origen creado con exito"});
                      }
                    }
                    else res.status(404).send("Error creando movimiento");
                  }
                );
              }
              else res.status(404).send("Error creando movimiento");
            }
            else res.status(404).send("Error creando movimiento");
          });
        }else{
          res.status(404).send("No existe la cuenta al que se le quiere asociar el movimiento");
        }
      }
      else res.status(404).send("Error creando movimiento");
    });

console.log("segundo movimiento de traspaso")

    if (req.body.tipo_movimiento == "traspaso")
    {
    //inserto el movimiento duplicado en la cuenta destino
    var query = 'q={"id_cuenta":' + parseInt(idCuentaDestino) + '}&f={"id_cuenta":1,"saldo":1, "_id":1}';
    var clienteMlab = requestJson.createClient(baseMLabURL + "/cuenta?" + query + "&l=1&" + mLabAPIKey);
    clienteMlab.get('', function(err, resM, body)
      {
        if(!err)
        {
          if(body.length == 1)
          {
            var idCuenta = body[0]._id.$oid;
            console.log("Se procede al registro del movimiento en la cuenta existente");
            var query = 'q={"_id":{$exists: true }}&f={"id_movimiento":1,"cuenta_id":1}&s={"id_movimiento":-1}&l=1';
            var saldoCuenta = body[0].saldo;
            console.log();

            //Consigue en max id ordenando descendentemente
            clienteMlab = requestJson.createClient(baseMLabURL + "/movimiento?" + query + "&" + mLabAPIKey);
            clienteMlab.get('', function(errY, resY, bodyY)
            {
              if(!errY)
              {
                if(bodyY.length == 1)
                {
                  var idNuevoMovimiento2 = bodyY[0].id_movimiento + 1;
                  console.log("idNuevoMovimiento:" + idNuevoMovimiento2);
                  bodyMovementDestino.id_movimiento = idNuevoMovimiento2;
                  var saldoMovimiento = bodyMovementDestino.importe;

                  console.log("importe movimiento " + saldoMovimiento);
                  console.log("saldo de la cuenta " + saldoCuenta);

                  clienteMlab = requestJson.createClient(baseMLabURL);

                  clienteMlab.post("movimiento?" + mLabAPIKey, bodyMovementDestino,
                    function (errP, resP, bodyP){
                      if(!errP)
                      {
                        console.log("Movimiento creado con exito");

                        //Actualizo el saldo de la cuenta afectada
                        clienteMlab = requestJson.createClient(baseMLabURL);
                        var saldoFinal = saldoMovimiento + saldoCuenta;

                        console.log("Saldo Cuenta Antes del movimiento"+ saldoCuenta);
                        console.log("Saldo Cuenta Update es "+ saldoFinal.toString());

                        var putBody = '{"$set" : {"saldo":'+ saldoFinal.toString() +'}}';

                        console.log(putBody);
                        console.log("la cuenta que se actualiza tiene id "+idCuenta);

                        clienteMlab.put("cuenta/" + idCuenta + "?"+ mLabAPIKey, JSON.parse(putBody),
                        function (errP2, resP2, bodyP2)
                        {
                          if(!errP2)
                          {
                            console.log("Cuenta actualizada con exito");

                          }else{
                            res.status(404).send("Error actualizando saldo de cuenta");
                          }
                        });
                        res.send({"msg" : "Movimiento traspaso creado con exito"});
                      }
                      else res.status(404).send("Error creando movimiento");
                    }
                  );
                }
                else res.status(404).send("Error creando movimiento");
              }
              else res.status(404).send("Error creando movimiento");
            });
          }else{
            res.status(404).send("No existe la cuenta al que se le quiere asociar el movimiento");
          }
        }
        else res.status(404).send("Error creando movimiento");
      });
    }
}

function deleteMovementV3(req, res)
{
  console.log("DELETE /apitechu/v3/movements/:id");
  console.log("id es " + req.params.id);

  var query = 'q={"id_movimiento":' + req.params.id + '}&f={"id_movimiento":1, "_id":1,"importe":1,"cuenta_id":1}';
  clienteMlab = requestJson.createClient(baseMLabURL + "/movimiento?" + query + "&l=1&" + mLabAPIKey);
  clienteMlab.get('', function(err, resM, body)
  {

    if(!err)
    {

      if(body.length == 1)
      {
        console.log("body: " + body.length);
        var id = body[0]._id.$oid;
        var saldoMovimiento = body[0].importe;
        var idCuenta = body[0].cuenta_id;
        console.log("id: " + id);
        console.log("saldo" + saldoMovimiento);
        clienteMlab = requestJson.createClient(baseMLabURL);
        console.log("Movement Delete");
        clienteMlab.delete("movimiento/" + id + "?"+ mLabAPIKey,
        function (errD, resD, bodyD)
        {
          if(!errD)
          {
            console.log("Movimiento borrado con exito");
            //El borrado no actualizará el saldo de la cuenta porque es para developers
            //actualizamos el saldo de la cuenta
            console.log("cuenta a actualizar" + idCuenta);
            var saldoFinal = 0;
            var query = 'q={"id_cuenta":' + parseInt(idCuenta) + '}&f={"id_cuenta":1,"saldo":1, "_id":1}';
            clienteMlab = requestJson.createClient(baseMLabURL + "/cuenta?" + query + "&l=1&" + mLabAPIKey);
            clienteMlab.get('', function(err, resM, bodyM)
              {
                if(!err)
                {
                  if(body.length == 1)
                  {
                    var idCuentaAct = bodyM[0]._id.$oid;
                    var saldoCuenta = bodyM[0].saldo;
                    if (Math.abs(saldoMovimiento)>Math.abs(saldoCuenta)){
                      //no borramos el movmiento
                    }else{
                      if (saldoMovimiento>0){
                        saldoFinal = saldoCuenta - saldoMovimiento
                      }else {
                        saldoFinal = saldoCuenta + (Math.abs(saldoMovimiento))
                      }
                    }


                    clienteMlab = requestJson.createClient(baseMLabURL);

                    console.log("Saldo Cuenta Antes del movimiento"+ saldoCuenta);
                    console.log("Saldo Cuenta Update es "+ saldoFinal.toString());

                    var putBody = '{"$set" : {"saldo":'+ saldoFinal.toString() +'}}';

                    console.log(putBody);
                    console.log("la cuenta que se actualiza tiene id "+idCuenta);

                    clienteMlab.put("cuenta/" + idCuentaAct + "?"+ mLabAPIKey, JSON.parse(putBody),
                    function (errP2, resP2, bodyP2)
                    {
                      if(!errP2)
                      {
                        console.log("Cuenta actualizada con exito");

                      }else{
                        res.status(404).send("Error actualizando saldo de cuenta");
                      }

                      console.log("Saldo de la cuenta actualizado");
                    });
                    res.send({"msg" : "Movimiento borrado con exito"})
                  }
                }
              }
            );
          }else{
            res.status(404).send("Error borrando Movimiento");
          }
        });
      }else{
        res.status(404).send("No existe el movimiento que se intenta borrar");
      }

    }else{
        res.status(404).send("se ha producido un error");
    }
  });
  }



module.exports.getMovementsV3 = getMovementsV3;
module.exports.getMovementByIdV3 = getMovementByIdV3;
module.exports.createMovementV3 = createMovementV3;
module.exports.deleteMovementV3 = deleteMovementV3;
