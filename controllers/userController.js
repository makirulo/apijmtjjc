const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/mongotechu/collections/";
const mLabAPIKey = "apiKey=6DgqdPnxkqpqkbiShQMs6QgujOamf0pV";


  function deleteUserV3(req, res) {
    console.log("DELETE /apitechu/v3/users/:id");
    console.log("id es " + req.params.id);

    var query = 'q={"id_usuario":' + req.params.id + '}&f={"id_usuario":1, "nombre":1, "apellido1":1, "apellido2":1, "_id":1}';
    clienteMlab = requestJson.createClient(baseMLabURL + "/usuario?" + query + "&l=1&" + mLabAPIKey);
    clienteMlab.get('', function(err, resM, body)
    {
      console.log("body: " + body.length);
      var id = body[0]._id.$oid;
      console.log("id: " + id);
      if(!err)
      {
        if(body.length == 1){
          //var query = 'q={"id_usuario":' + req.params.id + '}'
          clienteMlab = requestJson.createClient(baseMLabURL);
          console.log("User Delete");
          clienteMlab.delete("usuario/" + id + "?"+ mLabAPIKey,
		      function (errD, resD, bodyD){
            if(!errD)
            {
              console.log("Usuario borrado con exito");
              res.send({"msg" : "Usuario borrado con exito"})
            }else{
              res.status(404).send("Error borrando usuario");
            }});
          }else{
          res.status(404).send("No existe el usuario que se intenta borrar");
          }
        }
      }
    );
    }

    function updateUserV3(req, res) {
      console.log("POST /apitechu/v3/users/:id");
      console.log("id es " + req.params.id);
      console.log("nombre es " + req.body.nombre);
      console.log("apellido1 es " + req.body.apellido1);
      console.log("apellido2 es " + req.body.apellido2);
      console.log("telefono es " + req.body.telefono);
      console.log("dni es " + req.body.dni);
      console.log("domiciolio es " + req.body.domicilio);
      console.log("email es " + req.body.email);
      //console.log("password es " + req.body.password);


      var query = 'q={"id_usuario":' + req.params.id + '}&f={"id_usuario":1, "nombre":1, "apellido1":1, "apellido2":1, "_id":1}';
      clienteMlab = requestJson.createClient(baseMLabURL + "/usuario?" + query + "&l=1&" + mLabAPIKey);
      clienteMlab.get('', function(err, resM, body)
      {
        console.log("body: " + body.length);
        var id = body[0]._id.$oid;
        console.log("id: " + id);
        if(!err)
        {
          if(body.length == 1){
            //var query = 'q={"id_usuario":' + req.params.id + '}'
            clienteMlab = requestJson.createClient(baseMLabURL);
            console.log("User Update");
            var putBody = '{"$set" : {"nombre":"'+ req.body.nombre +'","apellido1":"'+ req.body.apellido1 +'","apellido2":"'+ req.body.apellido2 +'","telefono":"'+ req.body.telefono +'","dni":"'+ req.body.dni +'","domicilio":"'+ req.body.domicilio +'","email":"'+ req.body.email +'"}}';
            clienteMlab.put("usuario/" + id + "?"+ mLabAPIKey, JSON.parse(putBody),
  		      function (errP, resP, bodyP){
              if(!errP)
              {
                console.log("Usuario actualizado con exito");
                res.send({"msg" : "Usuario actualizado con exito"});
              }else{
                res.status(404).send("Error actualizando usuario");
              }});
            }else{
            res.status(404).send("No existe el usuario que se intenta actualizar");
            }
          }
        }
      );
      }

  /*Solucion 1 del profe*/
  /*for (user of users){
    console.log("Length of array is " + users.length);
    if (user != null && user.id == req.params.id){
      console.log("La id coincide");
      delete users[user.id - 1];
      break;
    }
  }*/

  /*Solucion 2 profe*/
  /*for (arrayId in users) {
   console.log("posición del array es " + arrayId);
   if (users[arrayId].id == req.params.id) {
     console.log("La id coincide");
     users.splice(arrayId, 1);
     break;
   }
 }*/

  /*Solucion 3 profe, el forEach no tiene break*/
  /*users.forEach(function (user, index) {
  if (user.id == req.params.id) {
    console.log("La id coincide");
    users.splice(index, 1);
  }
  });*/



/////VERSION3/////

function getUsersV3(req, res){
  console.log("GET /apitechu/v3/users");

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client Create");
  httpClient.get("usuario?" + mLabAPIKey,
    function (err, resMLab, body){
      var response = !err ? body : {
        "msg" : "Error obteniendo usuarios."
      }
      res.send(response);
    }
  );
}

function getUserByIdV3(req, res){
  console.log("GET /apitechu/v3/users/:id");

  var id = req.params.id;
  //'' comillas simples es un String literal
  var query  = 'q={"id_usuario":' + id + '}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client Create");
  httpClient.get("usuario?" + query + "&" + mLabAPIKey,
    function (err, resMLab, body){
      if (err) {
        response = {
          "msg" : "Error obteniendo usuario"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body[0];
          //Por defecto el status es 200
        } else {
          var response = {
            "msg" : "Usuario no encontrado"
          }
          res.status(404);
        }
      }
      /*var response = !err ? body : {
        "msg" : "Error obteniendo usuario."
      }*/
      res.send(response);
    }
  );
}

function getAccountV3(req, res){
  console.log("GET /apitechu/v3/users/:id/accounts");

  var id = req.params.id;
  //'' comillas simples es un String literal
  var query  = 'q={"usuario_id":' + id + '}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("query:" + query);
  httpClient.get("cuenta?" + query + "&" + mLabAPIKey,
    function (err, resMLab, body){
      if (err) {
        response = {
          "msg" : "Error obteniendo cuentas"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body;
          //Por defecto el status es 200
        } else {
          var response = {
            "msg" : "Cuenta/s no encontrada/s"
          }
          res.status(404);
        }
      }
      /*var response = !err ? body : {
        "msg" : "Error obteniendo usuario."
      }*/
      res.send(response);
    }
  );
}

function createUserV3(req, res) {
  console.log("POST /apitechu/v3/users");

  console.log("nombre " + req.body.nombre);
  console.log("apellido1 " + req.body.apellido1);
  console.log("apellido2 " + req.body.apellido2);
  console.log("email " + req.body.email);
  console.log("password " + req.body.password);
  console.log("telefono " + req.body.telefono);
  console.log("dni " + req.body.dni);
  console.log("domicilio " + req.body.domicilio);
  //console.log("password es " + req.body.password);

  var newUser = {
    "id_usuario" : parseInt("0"),
    "nombre" : req.body.nombre,
    "apellido1" : req.body.apellido1,
    "apellido2" : req.body.apellido2,
    "email" : req.body.email,
    "password" : crypt.hash(req.body.password),
    "telefono" : req.body.telefono,
    "dni" : req.body.dni,
    "domicilio" : req.body.domicilio
  };

  //console.log("nombre " + newUser.nombre);
  //newUser.id_usuario = parseInt("32");

  var query = 'q={"email":"' + req.body.email + '"}&f={"id_usuario":1, "nombre":1, "apellido1":1, "apellido2":1, "_id":0}';
    clienteMlab = requestJson.createClient(baseMLabURL + "/usuario?" + query + "&l=1&" + mLabAPIKey);
    clienteMlab.get('', function(err, resM, body)
    {
      if(!err)
    {
        if(body.length == 1)
        {
          res.status(404).send("Ya existe usuario con ese email");
        }
        else
        {
          console.log("Se procede al registro");
          var query = 'q={"password":{$exists: true }}&f={"id_usuario":1}&s={"id_usuario":-1}&l=1';  //Consigue en max id ordenando descendentemente
          clienteMlab = requestJson.createClient(baseMLabURL + "/usuario?" + query + "&" + mLabAPIKey);
          clienteMlab.get('', function(errX, resX, bodyX)
          {
            if(!errX)
            {
              if(bodyX.length == 1)
              {
                var id = bodyX[0].id_usuario +1;
                console.log(id);
                newUser.id_usuario = id;
                //Queremos insertar un documento (INSERT DOCUMENT) ver la documentacion del API de MLAB
                clienteMlab = requestJson.createClient(baseMLabURL);
                console.log("User Create");
                clienteMlab.post("usuario?" + mLabAPIKey, newUser,
                  function (errP, resP, bodyP){
                    if(!errP)
                    {
                      console.log("Usuario creado con exito");
                      res.send({"msg" : "Usuario creado con exito"})
                    }
                    else res.status(404).send("Error creando usuario");
                  }
                );
              }
              else res.status(404).send("Error creando usuario");
            }
            else res.status(404).send("Error creando usuario");
          });
        }
      }
      else res.status(404).send("Error creando usuario");
    });
}



module.exports.deleteUserV3 = deleteUserV3;
module.exports.updateUserV3 = updateUserV3;

module.exports.getUsersV3 = getUsersV3;
module.exports.getUserByIdV3 = getUserByIdV3;
module.exports.getAccountV3 = getAccountV3;
module.exports.createUserV3 = createUserV3;
