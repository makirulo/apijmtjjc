const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/mongotechu/collections/";
const mLabAPIKey = "apiKey=6DgqdPnxkqpqkbiShQMs6QgujOamf0pV";


/////VERSION3/////

function getAccountsV3(req, res){
  console.log("GET /apitechu/v3/accounts");

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Accounts");
  httpClient.get("cuenta?" + mLabAPIKey,
    function (err, resMLab, body){
      var response = !err ? body : {
        "msg" : "Error obteniendo cuentas."
      }
      res.send(response);
    }
  );
}

function getAccountByIdV3(req, res){
  console.log("GET /apitechu/v3/accounts/:id");

  var id = req.params.id;
  //'' comillas simples es un String literal
  var query  = 'q={"id_cuenta":' + id + '}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Account");
  httpClient.get("cuenta?" + query + "&" + mLabAPIKey,
    function (err, resMLab, body){
      if (err) {
        response = {
          "msg" : "Error obteniendo cuenta"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body[0];
          //Por defecto el status es 200
        } else {
          var response = {
            "msg" : "Cuenta no encontrada"
          }
          res.status(404);
        }
      }
      /*var response = !err ? body : {
        "msg" : "Error obteniendo usuario."
      }*/
      res.send(response);
    }
  );
}

function getAmountExChangeByIdV3(req, res){
  console.log("GET /apitechu/v3/accounts/:id/amount/:currency");

  var id = req.params.id;
  var currency = req.params.currency;
  //'' comillas simples es un String literal
  var query  = 'q={"id_cuenta":' + id + '}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("amount");
  httpClient.get("cuenta?" + query + "&" + mLabAPIKey,
    function (err, resMLab, body){
      if (err) {
        response = {"msg" : "Error obteniendo cuenta"}
        res.status(500);
      } else {
        if (body.length > 0) {
          //var response = body[0];
          //Por defecto el status es 200
          var clienteAPIext = requestJson.createClient("https://www.amdoren.com/api/currency.php");
          console.log("Amount ExChange");
          console.log(body[0].divisa);
          console.log(body[0].saldo);
          console.log(currency);
          console.log("?from=" + body[0].divisa + "&to=" + currency + "&amount=" + body[0].saldo + "&api_key=nAkDb4aW2Rgn4RsHYi7fE6dfwNjuAi");
          clienteAPIext.get("?from=" + body[0].divisa + "&to=" + currency + "&amount=" + body[0].saldo + "&api_key=nAkDb4aW2Rgn4RsHYi7fE6dfwNjuAi",
            function (errG, resG, bodyG){
              if(!errG)
              {
                console.log("Tipo de cambio exito");
                console.log("Saldo en " + currency + ": " + bodyG.amount);
                var response = bodyG;
                res.send(response);
              }
              else res.status(404).send("Error realizando el tipo de cambio");
            }
          );
        } else {
          var response = {"msg" : "Cuenta no encontrada"}
          res.status(404);
        }
      }
      //res.send(response);
    }
  );
}

function getMovementV3(req, res){
  console.log("GET /apitechu/v3/accounts/:id/movements");

  var id = req.params.id;
  //'' comillas simples es un String literal
  var query  = 'q={"cuenta_id":' + id + '}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("query:" + query);
  httpClient.get("movimiento?" + query + "&" + mLabAPIKey,
    function (err, resMLab, body){
      if (err) {
        response = {
          "msg" : "Error obteniendo movimientos"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body;
          //Por defecto el status es 200
        } else {
          var response = {
            "msg" : "Movimiento/s no encontrado/s"
          }
          res.status(200);
        }
      }
      /*var response = !err ? body : {
        "msg" : "Error obteniendo usuario."
      }*/
      res.send(response);
    }
  );
}
function creaIBAN(){

  var parte1 = Math.floor(Math.random() * (100 - 10)) + 10;
  var parte2 = Math.floor(Math.random() * (10000 - 1000)) + 1000;
  var parte3 = Math.floor(Math.random() * (10000 - 1000)) + 1000;
  var parte4 = Math.floor(Math.random() * (10000 - 1000)) + 1000;
  var parte5 = Math.floor(Math.random() * (10000 - 1000)) + 1000;

  return "ES"+parte1+" "+parte2+" "+parte3+" "+parte4+" "+parte5;

}
function createAccountV3(req, res) {
  console.log("POST /apitechu/v3/accounts/:id");

  var id = req.params.id;
  var iban = creaIBAN();
  console.log("id_usuario: " + id);
  console.log("mi iban es "+iban );
  var newAccount = {
    "id_cuenta" : parseInt("0"),
    //Realizar un generador de IBAN aleatorios
    "iban" : iban,
    "saldo" : parseInt("0"),
    "divisa" : "EUR",
    "tipo" : "Ahorro",
    "usuario_id" : parseInt(id)
  };


  var query = 'q={"id_usuario":' + parseInt(id) + '}&f={"id_usuario":1, "_id":0}';
    clienteMlab = requestJson.createClient(baseMLabURL + "/usuario?" + query + "&l=1&" + mLabAPIKey);
    console.log(baseMLabURL + "/cuenta?" + query + "&l=1&" + mLabAPIKey);
    clienteMlab.get('', function(err, resM, body)
    {
      if(!err)
    {
        if(body.length == 1)
        {
          console.log("Se procede al registro");
          var query = 'q={"usuario_id":{$exists: true }}&f={"id_cuenta":1}&s={"id_cuenta":-1}&l=1';  //Consigue en max id ordenando descendentemente
          clienteMlab = requestJson.createClient(baseMLabURL + "/cuenta?" + query + "&" + mLabAPIKey);
          clienteMlab.get('', function(errX, resX, bodyX)
          {
            if(!errX)
            {
              if(bodyX.length == 1)
              {
                var id = bodyX[0].id_cuenta +1;
                console.log("idCuenta:" + id);
                newAccount.id_cuenta = id;
                //Queremos insertar un documento (INSERT DOCUMENT) ver la documentacion del API de MLAB
                clienteMlab = requestJson.createClient(baseMLabURL);
                console.log("Account Create");
                clienteMlab.post("cuenta?" + mLabAPIKey, newAccount,
                  function (errP, resP, bodyP){
                    if(!errP)
                    {
                      console.log("Cuenta creada con exito");
                      res.send({"msg" : "Cuenta creada con exito"})
                    }
                    else res.status(404).send("Error creando cuenta");
                  }
                );
              }
              else res.status(404).send("Error creando cuenta");
            }
            else res.status(404).send("Error creando cuenta");
          });
        }else{
          res.status(404).send("No existe el usuario al que se le quiere asociar la cuenta");
        }
      }
      else res.status(404).send("Error creando cuenta");
    });
}

function deleteAccountV3(req, res) {
  console.log("DELETE /apitechu/v3/accounts/:id");
  console.log("id es " + req.params.id);

  var query = 'q={"id_cuenta":' + req.params.id + '}&f={"id_cuenta":1, "_id":1}';
  clienteMlab = requestJson.createClient(baseMLabURL + "/cuenta?" + query + "&l=1&" + mLabAPIKey);
  clienteMlab.get('', function(err, resM, body)
  {
    console.log("body: " + body.length);
    var id = body[0]._id.$oid;
    console.log("id: " + id);
    if(!err)
    {
      if(body.length == 1){
        //var query = 'q={"id_usuario":' + req.params.id + '}'
        clienteMlab = requestJson.createClient(baseMLabURL);
        console.log("Account Delete");
        clienteMlab.delete("cuenta/" + id + "?"+ mLabAPIKey,
        function (errD, resD, bodyD){
          if(!errD)
          {
            console.log("Cuenta borrada con exito");
            res.send({"msg" : "Cuenta borrada con exito"})
          }else{
            res.status(404).send("Error borrando cuenta");
          }});
        }else{
        res.status(404).send("No existe la cuenta que se intenta borrar");
        }
      }
    }
  );
  }



module.exports.getAccountsV3 = getAccountsV3;
module.exports.getAccountByIdV3 = getAccountByIdV3;
module.exports.getAmountExChangeByIdV3 = getAmountExChangeByIdV3;
module.exports.getMovementV3 = getMovementV3;
module.exports.createAccountV3 = createAccountV3;
module.exports.deleteAccountV3 = deleteAccountV3;
