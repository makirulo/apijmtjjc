//Inicializamos el framework en esta variables constante
const express = require('express');
const app = express();
const port = process.env.PORT || 3000

const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.listen(port);
console.log("API escuchando en el puerto " + port);

const userController = require('./controllers/userController');
const authController = require('./controllers/authController');
const accountController = require('./controllers/accountController');
const movementController = require('./controllers/movementController');

app.get('/apitechu/v1/hello',
// Funcion manejadora con request y response
  function(req, res){
    console.log("GET /apitechu/v1/hello");

    res.send({"msg" : "Hola desde API TechU con el cambio de node-modules"});
  }
);

app.post("/apitechu/v1/monstruo/:p1/:p2",
  function (req, res){
    console.log("POST /apitechu/v1/monstruo/:p1/:p2");

    console.log("Parametros");
    console.log(req.params);

    console.log("Query Strings");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);

  }
);




app.post('/apitechu/v3/login', authController.loginV3);
app.post('/apitechu/v3/logout/:id', authController.logoutV3);

//app.get('/apitechu/v2/users/:id/accounts', userController.getAccountV2);

app.get('/apitechu/v3/users', userController.getUsersV3);
app.get('/apitechu/v3/users/:id', userController.getUserByIdV3);
app.get('/apitechu/v3/users/:id/accounts', userController.getAccountV3);
app.post('/apitechu/v3/users', userController.createUserV3);
app.delete('/apitechu/v3/users/:id', userController.deleteUserV3);
app.post('/apitechu/v3/users/:id', userController.updateUserV3);

app.get('/apitechu/v3/accounts', accountController.getAccountsV3);
app.get('/apitechu/v3/accounts/:id', accountController.getAccountByIdV3);
app.get('/apitechu/v3/accounts/:id/amount/:currency', accountController.getAmountExChangeByIdV3);
app.get('/apitechu/v3/accounts/:id/movements', accountController.getMovementV3);
app.post('/apitechu/v3/accounts/:id', accountController.createAccountV3);
app.delete('/apitechu/v3/accounts/:id', accountController.deleteAccountV3);

app.get('/apitechu/v3/movements', movementController.getMovementsV3);
app.get('/apitechu/v3/movements/:id', movementController.getMovementByIdV3);
app.post('/apitechu/v3/movements',movementController.createMovementV3);
app.delete('/apitechu/v3/movements/:id', movementController.deleteMovementV3);
