const bcrypt = require('bcrypt');

function hash(data) {
  console.log("Hashing data");
  //10 las rondas de SALT
  return bcrypt.hashSync(data, 10);
}

function checkPassword(sentPassword, userHashedPassword) {
  console.log("Checking password");
  return bcrypt.compareSync(sentPassword, userHashedPassword);
}

module.exports.hash = hash;
module.exports.checkPassword = checkPassword;
