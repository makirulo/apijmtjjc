const mocha = require ('mocha');
const chai = require ('chai');
const chaihttp = require ('chai-http');

//Queremos probar los endpoint
chai.use(chaihttp);

//Permite assertions
var should = chai.should();

var server = require('../server');

//Dentro de un describe puedes haber varios it
//Nombre de la suit (describe)
describe('First unit test',
  function(){
    //Nombre del test unitario (it)
    it('Test that Duckduckgo works', function (done){
        chai.request('http://www.duckduckgo.com')
        .get('/')
        //Cuando responda asincronamente se realiza la function del end
        .end(
          function (err, res){
            console.log("Request has finished");
            //console.log(err);
            //console.log(res);
            res.should.have.status(200);
            //Aqui le indicas que ha terminado
            done();
          }
        )
      }
    )
  }
)

////////////////////////

describe('Test de API Usuarios',
  function(){
    //Nombre del test unitario (it)
    it('Prueba que la API de Usuarios responde', function (done){
        chai.request('http://localhost:3000')
        .get('/apitechu/v1/hello')
        //Cuando responda asincronamente se realiza la function del end
        .end(
          function (err, res){
            console.log("Request has finished");
            res.should.have.status(200);
            done();
          }
        )
      }
    ),
    it('Prueba que la API devuelve una lista de usuarios correcta', function (done){
        chai.request('http://localhost:3000')
        .get('/apitechu/v1/users')
        //Cuando responda asincronamente se realiza la function del end
        .end(
          function (err, res){
            console.log("Request has finished");
            res.should.have.status(200);
            res.body.users.should.be.a("array");

            for (user of res.body.users) {
              user.should.have.property('email');
              user.should.have.property('password');
            }
            done();
          }
        )
      }
    )
  }
)
